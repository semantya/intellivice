#!/usr/bin/env python

from autobahn.asyncio.websocket import WebSocketServerProtocol, WebSocketServerFactory

###############################################################################

class SocketsServer(WebSocketServerProtocol):
    def onConnect(self, request):
        print("\t-> Client connecting : {0}".format(request.peer))

    def onOpen(self):
        print("*) Socket open ...")

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
        else:
            print("Text message received: {0}".format(payload.decode('utf8')))

        self.sendMessage(payload, isBinary)

    def onClose(self, wasClean, code, reason):
        print("*) ... and, socket closed : {0}".format(reason))


if __name__ == '__main__':
    try:
        import asyncio
    except ImportError:
        # Trollius >= 0.3 was renamed
        import trollius as asyncio

    factory = WebSocketServerFactory("ws://streams.scubadev.com:9000", debug=False)
    factory.protocol = SocketsServer

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, '0.0.0.0', 9000)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
