#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

################################################################################

import RPi.GPIO as GPIO

from vice.contrib.AdaFrut.RFID import MFRC522

class MifareProtocol(JarvisProtocol):
    def setup(self):
        # Create an object of the class MFRC522
        self.mifare = MFRC522.MFRC522()

    def loop(self):
        # Scan for cards
        (status,TagType) = self.mifare.MFRC522_Request(self.mifare.PICC_REQIDL)

        # If a card is found
        if status == self.mifare.MI_OK:
            print "Card detected"

        # Get the UID of the card
        (status,uid) = self.mifare.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == self.mifare.MI_OK:
            meta = {
                'card': {
                    'uid': uid,
                    'nrw': ' '.join([str(x) for x in uid] or ['']),
                    'tag': TagType,
                },
                'blocks': {},
            }

            # This is the default key for authentication
            key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

            # Select the scanned tag
            self.mifare.MFRC522_SelectTag(uid)

            # Authenticate
            status = self.mifare.MFRC522_Auth(self.mifare.PICC_AUTHENT1A, 8, key, uid)

            # Check if authenticated
            if status == self.mifare.MI_OK:
                for blockAddr in range(1, 8):
                    recvData = []
                    recvData.append(self.mifare.PICC_READ)
                    recvData.append(blockAddr)
                    pOut = self.mifare.CalulateCRC(recvData)
                    recvData.append(pOut[0])
                    recvData.append(pOut[1])
                    (status, backData, backLen) = self.mifare.MFRC522_ToCard(self.mifare.PCD_TRANSCEIVE, recvData)
                    if not(status == self.mifare.MI_OK):
                      print "Error while reading!"
                    i = 0
                    if len(backData) == 16:
                        meta['blocks'][blockAddr] = backData

                self.mifare.MFRC522_StopCrypto1()

                print "Authentication ok"
            else:
                print "Authentication error"

            meta['auth'] = {
                'challenge': key,
                'result':    (status==self.mifare.MI_OK),
            }

            self.package('rfid', meta)

    def rip(self):
        GPIO.cleanup()

################################################################################

if __name__ == '__main__':
    run_vice_protocol(MifareProtocol,
        endpoint = ('10.7.0.1', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = False,
    )
