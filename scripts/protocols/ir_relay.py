#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

################################################################################

##  ~/.lircrc  ##

#begin
#    button = 1
#    # what button is pressed on the remote
#    prog = jarvis
#    # program to handle this command
#    config = one, horse # configs are given to program as list
#end
#begin
#    button = 2
#    prog = jarvis
#    config = two
#end

import lirc

################################################################################

class InfraRedProtocol(JarvisProtocol):
    REMOTEs = ('technosat', 'DVR')

    def setup(self):
        #self.hook('ir-relay', self.on_command)

        self.socket = lirc.init("jarvis")

    def loop(self):
        cmd = lirc.nextcode() # press 1 on remote after this

        self.package('ir-relay', {
            'command': cmd,
        })

    def rip(self):
        lirc.deinit()

################################################################################

if __name__ == '__main__':
    run_vice_protocol(InfraRedProtocol,
        endpoint = ('10.7.0.1', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = False,
    )
