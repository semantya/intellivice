#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

################################################################################

import Adafruit_DHT

class WeatherProtocol(JarvisProtocol):
    DHT_mapping = {
        11:   Adafruit_DHT.DHT11,
        22:   Adafruit_DHT.DHT22,
        2302: Adafruit_DHT.AM2302,
    }

    #***************************************************************************

    def setup(self):
        self.DHT_pin  = 4
        self.DHT_type = 11


    ############################################################################

    def loop(self):
        h, t = Adafruit_DHT.read_retry(self.DHT_mapping[self.DHT_type], self.DHT_pin)

        if h is not None and t is not None:
            self.package('weather', {
                "temp": {
                    "value": t,
                    "unit": "C",
                },
                "humid": {
                    "value": h,
                    "unit": "%",
                },
            })

################################################################################

if __name__ == '__main__':
    run_vice_protocol(WeatherProtocol,
        endpoint = ('10.7.0.1', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = False,
    )
