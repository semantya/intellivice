#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

from vice.streams.common import *

################################################################################

class WeatherProtocol(JarvisComponent):
    IR_NAMESPACE = 'jarvis'
    IR_REMOTEs = ('technosat', 'DVR')

    #***************************************************************************

    def setup(self):
        self.socket = lirc.init(self.IR_NAMESPACE)

    #***************************************************************************

    def register(self):
        self.subscribe(u'jarvis.phenix.relai', self.ctrl_relai)

    #***************************************************************************

    def rip(self):
        lirc.deinit()

    ############################################################################

    def loop(self):
        try:
            import Adafruit_DHT

            h, t = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11, 4)
        except Exception as ex:
            h, t = None, None

        if h is None:
            h = randrange(0, 100, 0.1, float)

        if t is None:
            t = randrange(20, 40, 0.01, float)

        p = randrange(50, 1000, 1, int)

        yield self.publish('weather', {
            'temp': {
                'value': t,
                'unit': '°',
            },
            'humid': {
                'value': h,
                'unit': '%',
            },
            'power': {
                'value': p,
                'unit': 'kWh',
            },
        })

        #***********************************************************************

        cmd = lirc.nextcode() # press 1 on remote after this

        yield self.publish('ir-relay', *cmd)

    ############################################################################

    def ctrl_relai(self, key, state):
        print("Got event: {}".format(i))
        self.received += 1
        if self.received > 5:
            self.leave()

    #***************************************************************************

    def ctrl_led(self, pin, acte):
        pass

################################################################################

if __name__ == '__main__':
    run_vice_component(WeatherProtocol,
        endpoint = ('10.7.0.1', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = False,
    )
