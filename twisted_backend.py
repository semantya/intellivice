from twisted.internet import reactor
from twisted.internet.defer import inlineCallbacks

from autobahn.twisted.wamp import ApplicationSession


class Component(ApplicationSession):

    """
    An application component that subscribes and receives events,
    and stop after having received 5 events.
    """

    @inlineCallbacks
    def onJoin(self, details):
        print("session attached")

        self.received = 0

        def on_event(i):
            print("Got event: {}".format(i))
            self.received += 1
            if self.received > 5:
                self.leave()

        yield self.subscribe(on_event, 'com.myapp.topic1')

    def onDisconnect(self):
        print("disconnected")
        reactor.stop()

if __name__ == '__main__':
    from autobahn.twisted.wamp import ApplicationRunner

    runner = ApplicationRunner("ws://streams.scubadev.com:9000", "apis.scubadev.com", debug=True)

    runner.run(Component)
