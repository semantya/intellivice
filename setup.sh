#!/bin/bash

apt-get update
apt-get install -y aptitude python-{dev,software-properties,setuptools,virtualenv} cython build-essential curl lib{event,mysqlclient,xml2,xslt1}-dev

add-apt-repository -y ppa:chris-lea/node.js

aptitude update
aptitude -y safe-upgrade

PKGs="htop traceroute nmap"
PKGs=$PKGs" lirc-x lcdproc conky-std"
#PKGs=$PKGs" nodejs"
PKGs=$PKGs" memcached redis-server "`echo python-{memcache,redis}`

aptitude install -y $PKGs

for svc in redis-server ; do
  update-rc.d -f $svc remove
done

################################################################################

cd /vice

pip install -r requirements.txt

#npm install

################################################################################

cp -afR /vice/etc/* /etc/

################################################################################

#reboot
