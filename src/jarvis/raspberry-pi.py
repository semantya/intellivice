#!/usr/bin/env python

import os, sys ; sys.path.append('/vice/lib/python')

#*******************************************************************************

from sparkle.shortcuts import *

mgr = Manager('phenix')

################################################################################

station = mgr.watch('rpi-weather', 'phenix',
    survey_pins=[
        (11, 'movement'),
    ],
    dht={
        'model': 11,
        'pin':    4,
    },
    lcd_addr=(0x21,1),
    rfid_model='mfrc522',
    #i2c=('lcd'),
    #spi=('mfrc522'),
)

#*******************************************************************************

@station.hook
def process_alarm(dog, **params):
    print "{alarm}\t%s" % repr(params)

#*******************************************************************************

@station.hook
def process_rfid(dog, block0):
    print "{rfid}\t%s" % block0

#*******************************************************************************

@station.hook
def process_weather(dog, **params):
    for key in params:
        params[key] = float(params[key])

    print "{weather}\t%s" % repr(params)

    dog.report_data('weather', **params)

################################################################################

if __name__=='__main__':
    mgr.asyncloop()
