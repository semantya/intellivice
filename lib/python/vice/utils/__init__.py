#-*- coding: utf-8 -*-

from vice.helpers import *

from vice.core.abstract import *

from vice.streams.common import *

################################################################################

def run_vice_protocol(wrapper, endpoint=('127.0.0.1',9000), realm=None, debug=False):
    if not realm:
        realm = realm or "ws://localhost:%d" % endpoint[1]

    factory = WebSocketClientFactory(realm, debug=debug)
    factory.protocol = wrapper

    loop = asyncio.get_event_loop()
    coro = loop.create_connection(factory, endpoint[0], int(endpoint[1]))

    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()

################################################################################

def run_vice_component(wrapper, endpoint=('127.0.0.1',9000), realm=None, debug=False):
    setattr(wrapper, "REALM", realm or ('ws://%s:%d' % endpoint))

    # 1) create a WAMP application session factory
    factory = wamp.ApplicationSessionFactory()
    factory.session = wrapper

    # 2) create a WAMP-over-WebSocket transport client factory
    transport = websocket.WampWebSocketClientFactory(factory, debug=False, debug_wamp=False)

    # 3) start the client
    loop = asyncio.get_event_loop()
    coro = loop.create_connection(transport, *endpoint)
    loop.run_until_complete(coro)

    # 4) now enter the asyncio event loop
    loop.run_forever()
    loop.close()
