#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys ; sys.path.append('/vice/lib/python')

from vice.shortcuts import *

from vice.streams.interfaces.Jarvis import *

################################################################################

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio

from autobahn.asyncio.wamp import ApplicationSession

################################################################################

class Component(ApplicationSession):

    """
    An application component that subscribes and receives events,
    and stop after having received 5 events.
    """

    @asyncio.coroutine
    def onJoin(self, details):

        self.received = 0

        def on_event(i):
            print("Got event: {}".format(i))
            self.received += 1
            if self.received > 5:
                self.leave()

        yield self.subscribe(on_event, 'com.myapp.topic1')

    def onDisconnect(self):
        asyncio.get_event_loop().stop()

################################################################################

if __name__ == '__main__':
    run_vice_protocol(DummyProtocol,
        endpoint = ('streams.scubadev.com', 9000),
        realm    = "ws://streams.scubadev.com:9000",
        debug    = False,
    )
